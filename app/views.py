#!/usr/bin/python
# -*- coding: latin-1 -*-

from flask import redirect, session, url_for, request, g, render_template
from flask.ext.login import login_user, logout_user, current_user, login_required
from flask.ext.sqlalchemy import get_debug_queries
from app import app, db, lm
from models import User, ROLE_USER, ROLE_ADMIN, Post, Bet
from datetime import datetime
from config import MAX_SEARCH_RESULTS, LANGUAGES, DATABASE_QUERY_TIMEOUT, WHOOSH_ENABLED
import time
import json
import api
import updater

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))
    
@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        for bet in g.user.betted:
            if not bet.expired:
                pic = bet.pic
                pic_info = api.pic_info(pic.id, g.user.access_token)
                likes = pic_info["likes"]["count"]
                pic.likes = likes
                db.session.commit() 

@app.after_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= DATABASE_QUERY_TIMEOUT:
            app.logger.warning("SLOW QUERY: %s\nParameters: %s\nDuration: %fs\nContext: %s\n" % (query.statement, query.parameters, query.duration, query.context))
    return response

@app.route('/', methods = ['GET'])
def index():
    return render_template("index.html")

@app.route("/debug_login")
def debug_login():
    user = User.query.filter_by(username = "davidhao3300").first()
    login_user(user, remember = True)
    return json.dumps({"user" : user.dict_version(), "access_token" : "1050839566.532a562.07583b2aa71c44039e8e490e940b8a65", "code" : 0, "message" : "Logged in"})

@app.route('/login')
def login(): 
    #if "auth" not in request.url:
    #    return json.dumps({"code":request.url.split("=")[1]})
    if "error" in request.url:
        return json.dumps({"code" : 2, "message" : "User did not go through with authorization for some reason."})   
    if "code" not in request.url:
        return redirect("https://api.instagram.com/oauth/authorize/?client_id=532a5629d03141ab929812cff97d0c00&redirect_uri=http://socialbookie.herokuapp.com/login&response_type=code")
    code = request.url.split("=")[1]
    """data = api.login(code)
    if not data:
        return json.dumps({"code" : 3, "message" : "OAuthException, contact me if this happens."})
    print data
    user = User.query.filter_by(username = data["user"]["username"]).first()
    if user is None:
        user = User(username = data["user"]["username"], 
            fullname = data["user"]["full_name"], 
            profile_pic = data["user"]["profile_picture"], 
            id = int(data["user"]["id"]), 
            about = data["user"]["bio"], 
            website = data["user"]["website"],
            role = ROLE_USER,
            access_token = data["access_token"],
            followers = data["user"]["counts"]["followed_by"],
            currency = 1000)
        db.session.add(user)
        db.session.commit()
    else:
        user.username = data["user"]["username"], 
        user.fullname = data["user"]["full_name"], 
        user.profile_pic = data["user"]["profile_picture"],  
        user.about = data["user"]["bio"], 
        user.website = data["user"]["website"],
        user.access_token = data["access_token"]
        user.followers = data["user"]["counts"]["followed_by"]
        db.session.add(user)
        db.session.commit()
    login_user(user, remember = True)
    return json.dumps({"user" : user.dict_version(), "access_token" : user.access_token, "code" : 0, "message" : "Logged in"})
    """
    return json.dumps({"code": code})
@app.route('/logout')
def logout():
    logout_user()
    g.user = None
    return json.dumps({"code" : 0, "message" : "Logged out."})


@app.route('/feed')
@login_required
def feed(): 
    data = api.feed(g.user.access_token)
    return json.dumps({"code" : 0, "message" : "user's feed", "feed":data})

@app.route('/user/<user_id>')
@login_required
def user(user_id):
    data = api.user_info(int(user_id), g.user.access_token)
    return json.dumps({"code":0, "message":"User info", "user": data})

@app.route("/status")
def status():
    if not g.user.is_authenticated():
        return json.dumps({"code" : 0, "status" : 0, "message" : "No user logged in."})
    else:
        return json.dumps({"code" : 0, "status" : 1, "message" : "User " + g.user.username + " logged in.", "user" : g.user.dict_version()})

@login_required
@app.route('/bet/<pic_id>/<amount>')
#@login_required
def bet(pic_id = "0", amount = 0, time = 24*60*60):
    if pic_id=="0" or amount == 0:
        return json.dumps({"code":10, "message" : "Not enough information in URL"})
    pic = Post.query.filter_by(id = pic_id).first()
    if pic is None:
        data = api.pic_info(pic_id, g.user.access_token)
        pic = Post(id = pic_id, link = data["images"]["standard_resolution"]["url"], small_link = data["images"]["thumbnail"]["url"], timestamp = int(data["created_time"]), poster = int(data["user"]["id"]))
    if g.user.has_betted(pic):
        return json.dumps({"code":11, "message" : "User already bet on this picture."})
    g.user.bet(pic, int(amount), int(time))
    return json.dumps({"code" : 0, "message" : "Bet on picture with id: "+pic_id})

@app.route("/debug_bet/<pic_id>/<amount>/<time>")
def debug_bet(pic_id, amount, time):
    pic = Post.query.filter_by(id = pic_id).first()
    if pic is None:
        data = api.pic_info(pic_id, g.user.access_token)
        pic = Post(id = pic_id, link = data["images"]["standard_resolution"]["url"], small_link = data["images"]["thumbnail"]["url"], timestamp = int(data["created_time"]), poster = int(data["user"]["id"]))
    if g.user.has_betted(pic):
        return json.dumps({"code" : 11, "message" : "User already bet on this picture"})
    g.user.bet(pic, int(amount), int(time))
    return json.dumps({"code" : 0, "message" : "Bet on picture with id: "+pic_id})

@app.route("/pic/<pic_id>")
@login_required
def pic_info(pic_id = "0"):
    if pic_id=="0":
        return json.dumps({"code":10, "message" : "Not enough information in URL"})
    return json.dumps({"code" : 0, "message" : "Picture info", "data" : api.pic_info(pic_id, g.user.access_token)})

@app.route("/average/<user_id>")
@login_required
def average(user_id = "0"):
    if user_id=="0":
        return json.dumps({"code":10, "message" : "Not enough information in URL"})
    data = api.average(user_id, g.user.access_token)
    
    return json.dumps({"code" : 0, "message" : "Average followers", "data" : data})

@app.route("/bet_feed")
@login_required
def bet_feed():
    bets = g.user.current_bets()
    to_return = json.dumps({"code" : 0, "bets": [bet.dict_version() for bet in bets]})
    for bet in bets:
        bet.new = False
    db.session.commit()
    return to_return
@app.route("/debug_start")
def start_timer():
    if updater.startTimer():
        return json.dumps({"code" : 0, "message" : "Timer started"})
    else:
        return json.dumps({"code" : 12, "message" : "Timer already started"})
        
@lm.unauthorized_handler
def unauthorized():
    return json.dumps({"code" : 100, "message" : "User not logged in."})